#include <stdio.h>
#include <darknet.h>

int main(int argc, char **argv)
{
  int w;
  int h;
  int c;
  float step;
  image im;
  char buff[256];

  w = 512;
  h = 512;
  c = 3;
  step = 0.1;

  if (argc < 2)
    {
      printf("usage: %s [option]\n", argv[0]);
      printf("Option:\t color\tgenerates color image\n");
      printf("Option:\t grey\tgenerates greyscale image\n");
      return (-1);
    }
  im = make_image(h, w, c);
  if (find_arg(argc, argv, "color"))
    {
      save_image(im, "black");
      for (int i = 0; i < h * w; ++i)
	im.data[i] = 1;
      save_image(im, "red");
      for (int i = h * w; i < h * w * 2; ++i)
	im.data[i] = 1;
      save_image(im, "yellow");
      for (int i = h * w * 2; i < h * w * 3; ++i)
	im.data[i] = 1;
      save_image(im, "white");
      for (int i = 0; i < h * w; ++i)
	im.data[i] = 0;
      save_image(im, "cyan");
      for (int i = h * w; i < h * w * 2; ++i)
	im.data[i] = 0;
      save_image(im, "blue");
      for (int i = 0; i < h * w; ++i)
	im.data[i] = 1;
      save_image(im, "magenta");
      for (int i = 0; i < h * w; ++i)
	im.data[i] = 0;
      for (int i = h * w * 2; i < h * w * 3; ++i)
	im.data[i] = 0;
      for (int i = h * w; i < h * w * 2; ++i)
	im.data[i] = 1;
      save_image(im, "green");
    }
  if (find_arg(argc, argv, "grey"))
    {
      for (float scale = 0; scale <= 1; scale += step)
	{
	  for (int i = 0; i < h * w * c; ++i)
	    im.data[i] = scale;
	  sprintf(buff, "grey_%d", (int)(scale * 10));
	  save_image(im, buff);
	}
    }
  else
    {
      printf("%s: Invalid option.\n", argv[0]);
    return (-1);
    }
  return (0);
}
