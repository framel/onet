#/ bin/bash

read -p "Dossier visiolab [visiolab] : " VLSDIR
if [ "$VLSDIR" == "" ]
then
    VLSDIR="visiolab"
fi
read -p "kit visiolab [invest] (invest/superv/full): " VLSKIT
if [ "$VLSKIT" == "" ]
then
    VLSDIR="invest"
fi
read -p "Login git : " GITLOG
read -sp "mot de passe git : " GITPWD

echo "Installing in $VLSDIR"
mkdir $VLSDIR

if [ "$VLSKIT" == "invest" ]
then
    git clone https://$GITLOG:$GITPWD@framagit.org/ouzb64ty/orizon.git $VLSDIR/orizon
    git clone https://$GITLOG:$GITPWD@framagit.org/ouzb64ty/vision.git $VLSDIR/vision
fi
if [ "$VLSKIT" == "superv" ]
then
    git clone https://$GITLOG:$GITPWD@framagit.org/ouzb64ty/orizon-supervision.git $VLSDIR/orizon
    git clone https://$GITLOG:$GITPWD@framagit.org/ouzb64ty/vision-supervision.git $VLSDIR/vision
fi
if [ "$VLSKIT" == "full" ]
then
    git clone https://$GITLOG:$GITPWD@framagit.org/ouzb64ty/orizon-supervision.git $VLSDIR/orizon
    git clone https://$GITLOG:$GITPWD@framagit.org/ouzb64ty/vision-supervision.git $VLSDIR/vision
fi
git clone https://gitlab.com/ouzb64ty/supnet.git $VLSDIR/vision/supnet
git clone https://github.com/DaveGamble/cJSON.git $VLSDIR/vision/supnet/cJSON
git clone https://$GITLOG:$GITPWD@framagit.org/framel/ozone.git $VLSDIR/vision/supnet/ozone
make -C -j4 $VLSDIR/vision/supnet/ozone
sudo cp $VLSDIR/vision/supnet/ozone/include/darknet.h /usr/include
sudo cp $VLSDIR/vision/supnet/ozone/libdarknet.so /usr/lib
make -C $VLSDIR/vision/supnet/cJSON
sudo mkdir /usr/include/cjson
sudo cp $VLSDIR/vision/supnet/cJSON/cJSON.h /usr/include/cjson
sudo cp $VLSDIR/vision/supnet/cJSON/libcjson.so /usr/lib
sudo cp $VLSDIR/vision/supnet/cJSON/libcjson.so.1 /usr/lib
make -C $VLSDIR/vision/supnet
mkdir -p $VLSDIR/vision/public/sequences
sudo apt install -y mongodb-server
sudo apt install -y redis-server
sudo apt install -y npm
sudo apt install -y ffmpeg
cd $VLSDIR/orizon
npm install
cd -
cd $VLSDIR/vision
npm install
cd ..

echo "Bienvenue sur Visiolab"
echo "Pour commencer copier votre fichier de poids dans $VLSDIR/vision/supnet/data"
